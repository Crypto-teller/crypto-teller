from .base import *
from .secure import *
from .packages import *
from decouple import config

DEBUG = config('DEBUG', cast=bool)
ALLOWED_HOSTS = config('ALLOWED_HOSTS', cast=lambda v: [s.strip() for s in v.split(',')])

# ######################## #
#     STATIC Compressor    #
# ######################## #
if config("COMPRESSOR", default=False, cast=bool):
    COMPRESS_ENABLED = True

    STATICFILES_FINDERS = (
        'django.contrib.staticfiles.finders.FileSystemFinder',
        'django.contrib.staticfiles.finders.AppDirectoriesFinder',
        # other finders..
        'compressor.finders.CompressorFinder',
    )


# ######################## #
#       HTML MINIFIER      #
# ######################## #
if config("HTML_MINIFIER", default=False, cast=bool):
    MIDDLEWARE.extend([
        'htmlmin.middleware.HtmlMinifyMiddleware',
        'htmlmin.middleware.MarkRequestMiddleware',
    ])

    HTML_MINIFY = True
    KEEP_COMMENTS_ON_MINIFYING = False
    CONSERVATIVE_WHITESPACE_ON_MINIFYING = True


