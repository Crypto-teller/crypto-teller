from .base import *
from .secure import *
from .packages import *
from decouple import config

DEBUG = True
ALLOWED_HOSTS = config('ALLOWED_HOSTS', cast=lambda v: [s.strip() for s in v.split(',')])

# ######################## #
#   DJANGO DEBUG TOOLBAR   #
# ######################## #
if config("DEBUG_TOOLBAR", default=False, cast=bool):
    INSTALLED_APPS.append('debug_toolbar')
    MIDDLEWARE.insert(0, 'debug_toolbar.middleware.DebugToolbarMiddleware')
    INTERNAL_IPS = config('CORS_ALLOWED_ORIGINS', cast=lambda v: [s.strip() for s in v.split(',')])

    def show_toolbar(request):
        return True
    DEBUG_TOOLBAR_CONFIG = {
        "SHOW_TOOLBAR_CALLBACK" : show_toolbar,
    }

# ######################## #
#     DJANGO EXTENSIONS    #
# ######################## #

INSTALLED_APPS.append('django_extensions')

SHELL_PLUS_IMPORTS = [
    "from django.db import connection as c",
]