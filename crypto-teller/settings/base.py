import os
from datetime import datetime
from decouple import config

BASE_DIR = os.path.normpath(
    os.path.join(os.path.dirname(os.path.dirname(
        os.path.abspath(__file__))), os.pardir)
)

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.AllowAllUsersModelBackend'
]

ROOT_URLCONF = 'crypto-teller.urls'
TEMPLATES_DIR = os.path.join(BASE_DIR, config('TEMPLATES_DIR'))

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [TEMPLATES_DIR, os.path.join(BASE_DIR, config('DIRS'))],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'string_if_invalid': 'ERROR: INVALID VARIABLE'
        },
    },
]

WSGI_APPLICATION = 'crypto-teller.wsgi.application'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

##############################
#    INTERNATIONALIZATION    #
##############################

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

################
#    STATIC    #
################

STATIC_URL = config('STATIC_URL')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, config('STATIC_DIR')),
)

STATIC_ROOT = os.path.join(BASE_DIR, config('COLLECT_STATIC_DIR'))

MEDIA_URL = config('MEDIA_URL')

MEDIA_ROOT = os.path.join(BASE_DIR, config('MEDIA_UPLOAD_DIR'))
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

FIXTURE_DIRS = tuple(
    config('FIXTURE_TEST_DIRS')
)

#################
#    LOGGING    #
#################
deployment_lvl = config('DEPLOYMENT_LEVEL')

if not os.path.exists(os.path.join(BASE_DIR, 'log')):
    os.makedirs(os.path.join(BASE_DIR, 'log'))

if not os.path.exists(os.path.join(BASE_DIR, 'log', 'default',f'{deployment_lvl}')):
    os.makedirs(os.path.join(BASE_DIR, 'log', 'default',f'{deployment_lvl}'))

date = datetime.today().strftime('%Y-%m-%d')

logging_handler = 'dev_handler' if deployment_lvl == 'stage' or deployment_lvl == 'dev' \
                                else 'prod_handler' if deployment_lvl == 'prod' \
                                else ''

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,

    'loggers': {
        'main': {
            'handlers': [
                logging_handler,
            ],
            'level': 'DEBUG',
        },
    },

    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
        'remove_base_dir_from_path': {
            '()': 'utils.log_filters.filters.PathFilter',
        },
        'dynamic_file_changer': {
            '()': 'utils.log_filters.filters.GetAppName'
        },
    },

    'handlers': {
        'dev_handler': {
            'level': 'DEBUG',
            'filters': ['dynamic_file_changer'],
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': f'./log/default/{deployment_lvl}/{date}.log',
            'when': 'D',
            'interval': 1,
            'formatter': 'detailed_formatter',
        },
        'prod_handler': {
            'level': 'WARNING',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': f'./log/default/{deployment_lvl}/{date}.log',
            'filters': ['dynamic_file_changer'],
            'when': 'D',
            'interval': 30,
            'formatter': 'detailed_formatter',
        }
    },
    'formatters': {
        'info_formatter': {
            'format': '{levelname} [{asctime}] [{message}]',
            'style': '{',
        },
        'detailed_formatter': {
            'format': '{levelname} [{asctime}] [func:{funcName}] {process:d} {thread:d} [{pathname}] [{message}]',
            'style': '{',
        },
    }
}
