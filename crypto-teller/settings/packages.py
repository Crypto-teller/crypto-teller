import os
from datetime import datetime
from datetime import timedelta

from .base import (
    INSTALLED_APPS,
    MIDDLEWARE,
    BASE_DIR,
)

from django.conf import settings

from django.utils.translation import gettext_lazy as _

from decouple import config


# #################### #
#  DJANGO EXTENTIONS   #
# #################### #
INSTALLED_APPS.append('django.contrib.sitemaps')
INSTALLED_APPS.append('django.contrib.sites')
INSTALLED_APPS.append('django.contrib.admindocs')
INSTALLED_APPS.append('django.contrib.humanize')
INSTALLED_APPS.append('django.contrib.postgres')

# #################### #
#   ADMIN EXTENTIONS   #
# #################### #
INSTALLED_APPS.append('admin_footer')
INSTALLED_APPS.append('admin_honeypot')
INSTALLED_APPS.append('django_admin_listfilter_dropdown')
INSTALLED_APPS.append('adminsortable2')
INSTALLED_APPS.insert(0,'admin_confirm')
INSTALLED_APPS.append('rangefilter')

# ############## #
#   EXTENSIONS   #
# ############## #

# Template
INSTALLED_APPS.append('widget_tweaks')

# Database Model
INSTALLED_APPS.append('eav')
INSTALLED_APPS.append('mptt')
INSTALLED_APPS.append('django_mptt_admin')

# Performance
INSTALLED_APPS.append('cacheops')
INSTALLED_APPS.append('compressor')
INSTALLED_APPS.append('sorl.thumbnail')

# Tracking
INSTALLED_APPS.append('safedelete')
INSTALLED_APPS.append('simple_history')

# Rest
INSTALLED_APPS.append('rest_framework')
INSTALLED_APPS.append('rest_framework_simplejwt')
INSTALLED_APPS.append('rest_framework_simplejwt.token_blacklist')
INSTALLED_APPS.append('drf_yasg')
INSTALLED_APPS.append('django_filters')
INSTALLED_APPS.append('corsheaders')

# ############## #
# CUSTOM PROJECT #
# ############## #
# INSTALLED_APPS.append('pages')
# INSTALLED_APPS.append('discount')
# INSTALLED_APPS.append('warehouse')
# INSTALLED_APPS.append('behavior')
# INSTALLED_APPS.append('cart')
# INSTALLED_APPS.append('blog')
# INSTALLED_APPS.append('notification')
# INSTALLED_APPS.append('comment')
# INSTALLED_APPS.append('ticket')
# INSTALLED_APPS.append('logistic')
# INSTALLED_APPS.append('newsletter')
# INSTALLED_APPS.append('user')
INSTALLED_APPS.append('bot')


# ############## #
#   MIDDLEWARE   #
# ############## #

# Get IP address of each request
MIDDLEWARE.append('utils.middleware.ip.GetIPMiddleware')
# Hide CSRF from Wappalyzer
MIDDLEWARE.append('utils.middleware.wisecsrf.CSRFHideMiddleware')
# Support CORS Origin for API
MIDDLEWARE.insert(2, "corsheaders.middleware.CorsMiddleware")
# Support Multiple Language
MIDDLEWARE.insert(3, 'django.middleware.locale.LocaleMiddleware')
# Track Record modification on database level
MIDDLEWARE.append('simple_history.middleware.HistoryRequestMiddleware')
# To add documentation support in django admin
MIDDLEWARE.append('django.contrib.admindocs.middleware.XViewMiddleware')


# ################## #
#     CORS ORIGIN    #
# ################## #
CORS_ALLOWED_ORIGINS = config('CORS_ALLOWED_ORIGINS', cast=lambda v: [s.strip() for s in v.split(',')])
CORS_ALLOW_ALL_ORIGINS = config('CORS_ALLOW_ALL_ORIGINS', cast=bool)

# ################ #
#   ADMIN FOOTER   #
# ################ #

ADMIN_FOOTER_DATA = {
    'site_url': config('SITE_BASE_URL'),
    'site_name': config('SITE_NAME'),
    'period': '{}'.format(datetime.now().year),
    'version': 'v0.1.0 - develop '
}

# #################### #
#    AUTHENTICATION    #
# #################### #
LOGIN_URL = '/account/login/'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'
AUTH_USER_MODEL = 'bot.BotUser'

# #################### #
#       Site map       #
# #################### #
SITE_ID = 1

# ################ #
#    Thumbnail     #
# ################ #
THUMBNAIL_DEBUG = config('THUMBNAIL_DEBUG', cast=bool)

THUMBNAIL_KVSTORE = 'sorl.thumbnail.kvstores.redis_kvstore.KVStore'
THUMBNAIL_ENGINE = 'sorl.thumbnail.engines.pil_engine.Engine'
THUMBNAIL_FORMAT='JPEG'
THUMBNAIL_REDIS_DB = config('REDIS_THUMBNAIL_DATABASE', cast=int)
THUMBNAIL_REDIS_PASSWORD=config('REDIS_PASSWORD')
THUMBNAIL_REDIS_HOST=config('REDIS_HOST')
THUMBNAIL_REDIS_PORT=config('REDIS_PORT')
THUMBNAIL_KEY_PREFIX=config('THUMBNAIL_KEY_PREFIX')

# ########################### #
#    INTERNATIONALIZATION     #
# ########################### #

if config('INTERNATIONALIZATION'):
    if not os.path.isdir(os.path.join(BASE_DIR, config('TRANSLATION_DIR'))):
        os.makedirs(os.path.join(BASE_DIR, config('TRANSLATION_DIR')))

# default language
LANGUAGE_CODE = config('DEFAULT_LANGUAGE')

# supported languages
LANGUAGES = (
    ("en", _("English")),
    ("fa", _("Persian")),
    ("ar", _("Arabic")),
)

LOCALE_PATHS = (
    os.path.join(BASE_DIR, config('TRANSLATION_DIR')),
)

# ############## #
# REST FRAMEWORK #
# ############## #

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_simplejwt.authentication.JWTAuthentication',
    ),
    # Versioning
    # 'DEFAULT_VERSIONING_CLASS': 'utils.api.utils.versioning.AcceptHeaderVersioningRequired',
    # 'ALLOWED_VERSIONS': ('1.0', '1.1'),
    # Pagination
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 100
}

if config('DEBUG', cast=bool):
    REST_FRAMEWORK['DEFAULT_RENDERER_CLASSES'] = [
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer'
    ]
else:
    REST_FRAMEWORK['DEFAULT_RENDERER_CLASSES'] = ['rest_framework.renderers.JSONRenderer']

# with open('.confidential/jwtRS256.key', 'r') as private_key, \
#     open('.confidential/jwtRS256.key.pub', 'r') as public_key:
#         PRIVATE_KEY = private_key.read()
#         PUBLIC_KEY = public_key.read()

SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(minutes=config('JWT_ACCESS_TOKEN_LIFETIME', cast=int)),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=config('JWT_REFRESH_TOKEN_LIFETIME', cast=int)),
    'ROTATE_REFRESH_TOKENS': config('JWT_ROTATE_REFRESH_TOKENS', cast=bool),
    'BLACKLIST_AFTER_ROTATION': config('JWT_BLACKLIST_AFTER_ROTATION', cast=bool),
    'UPDATE_LAST_LOGIN': config('JWT_UPDATE_LAST_LOGIN', cast=bool),

    'ALGORITHM': 'RS256',
    # 'SIGNING_KEY': PRIVATE_KEY,
    # 'VERIFYING_KEY': PUBLIC_KEY,
    'AUDIENCE': config('JWT_PAYLOAD_AUDIENCE'),
    'ISSUER': 'user',
    'JWK_URL': None,
    'LEEWAY': 0,

    'AUTH_HEADER_TYPES': config('JWT_AUTH_HEADER_TYPE', cast=tuple),
    'AUTH_HEADER_NAME': 'HTTP_AUTHORIZATION',
    'USER_ID_FIELD': 'email',
    'USER_ID_CLAIM': 'user_email',
    'USER_AUTHENTICATION_RULE': 'rest_framework_simplejwt.authentication.default_user_authentication_rule',

    'AUTH_TOKEN_CLASSES': ('rest_framework_simplejwt.tokens.AccessToken',),
    'TOKEN_TYPE_CLAIM': 'token_type',

    'JTI_CLAIM': 'jti',

    'SLIDING_TOKEN_REFRESH_EXP_CLAIM': 'refresh_exp',
    'SLIDING_TOKEN_LIFETIME': timedelta(minutes=config('JWT_SLIDING_TOKEN_LIFETIME', cast=int)),
    'SLIDING_TOKEN_REFRESH_LIFETIME': timedelta(days=config('JWT_SLIDING_TOKEN_REFRESH_LIFETIME', cast=int)),
}

# ######### #
#   CACHE   #
# ######### #

SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_CACHE_ALIAS = "default"

REDIS_AUTHENTICATION_URL = f"{config('REDIS_USER')}:{config('REDIS_PASSWORD')}"
REDIS_ADDRESS = f"{config('REDIS_HOST')}:{config('REDIS_PORT')}"
REDIS_DB = config("REDIS_DATABASE")
REDIS_TEST_DB = config("REDIS_TEST_DATABASE")
REDIS_PROTO = config('REDIS_PROTOCOL')

REDIS_TEST_CONNECTION_URL = f"{REDIS_PROTO}://{REDIS_AUTHENTICATION_URL}@{REDIS_ADDRESS}/{REDIS_TEST_DB}"
REDIS_CONNECTION_URL = f"{REDIS_PROTO}://{REDIS_AUTHENTICATION_URL}@{REDIS_ADDRESS}/{REDIS_DB}"

CACHEOPS_REDIS = REDIS_CONNECTION_URL

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": REDIS_CONNECTION_URL,
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "COMPRESSOR": "django_redis.compressors.zlib.ZlibCompressor",
        }
    },
    "test": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": REDIS_TEST_CONNECTION_URL,
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
        
    }
}

CACHEOPS = {
    'user.customuser': {'ops': 'get', 'timeout': 60*60},
    'warehouse.*': {'ops': 'all', 'timeout': 500},
    'comment.*': {'ops': 'all', 'timeout': 500},
    'pages.*': {'ops': 'all', 'timeout': 500},
}
