import os
import logging
from pathlib import Path
from datetime import datetime

from django.core.mail import send_mail
from django.conf import settings

from decouple import config

directory = settings.BASE_DIR
deployment_lvl = config('DEPLOYMENT_LEVEL')

class PathFilter(logging.Filter):

    def filter(self, record):
        if directory in record.pathname:
            record.pathname = record.pathname.replace(directory, '')
        return True


class GetAppName(logging.Filter):

    def check_max_size(self, record) -> None:
        """
        Check Maximum size of log volume. If volume exceed the max size,
        send email to adminstrators.
        """

        # MAXIMUM_SIZE unit: Byte
        MAXIMUM_SIZE = config('LIMIT_LOG_VOLUME_BYTE', cast=int)
        logger = logging.getLogger(f'{record.name}')

        for handler in logger.handlers:
            file_size = Path(handler.baseFilename).stat().st_size
            if file_size > MAXIMUM_SIZE:
                return True
        return False

    def create_new_log_file(self, handler):
        current_datetime = datetime.today().strftime('%Y-%m-%d--%H_%M_%S')

        handler.baseFilename = handler.baseFilename.replace(
            Path(handler.baseFilename).parts[-1],
            f'{current_datetime}.log')
        handler.doRollover()

    def send_mail(self, recepients=[]):
        # TODO NEED TO SEND EMAIL IF THIS CONDITION IS TRUE
        pass

    def create_dir(self, app_name ):
        if not os.path.exists(os.path.join(directory, 'log', f'{app_name}',deployment_lvl)):
            os.makedirs(os.path.join(directory, 'log', f'{app_name}',deployment_lvl))

    def filter(self, record):
        '''
        apps directory pattern in this case : BASE_DIR\\{APPNAMES}\\{RECORD_LEVEL}
        log directory pattern in this case : BASE_DIR\\logs\\{APPNAMES}\\{RECORD_LEVEL}
        '''
        logger = logging.getLogger(f'{record.name}')
        splitted_path = Path(logger.handlers[0].baseFilename).parts

        appname_index = splitted_path.index('log') + 1
        levelname_index = splitted_path.index('log') + 2

        current_handler_appname = splitted_path[4]
        current_handler_levelname = splitted_path[5]
        current_mixed = os.path.join(current_handler_appname, current_handler_levelname)
        record_appname = Path(record.pathname.replace(directory, '')).parts[1]

        appname_deploy_lvl = os.path.join(record_appname,deployment_lvl)

        # breakpoint()
        self.create_dir(record_appname)

        for handler in logger.handlers:
            if appname_deploy_lvl not in handler.baseFilename:
                handler.baseFilename = handler.baseFilename.replace(
                    current_mixed, appname_deploy_lvl)
                if not os.path.exists(handler.baseFilename):
                    handler.doRollover()

            is_log_exceed_volume = self.check_max_size(record)

            if is_log_exceed_volume:
                self.create_new_log_file(handler)

            if deployment_lvl == 'prod' and (record.levelname == 'WARNING' or record.levelname == 'ERROR' or record.levelname == 'CRITICAL'):
                self.submit_record_to_log(record, handler)
            elif deployment_lvl == 'stage' and record.levelname != 'DEBUG':
                self.submit_record_to_log(record, handler)
            elif deployment_lvl == 'dev':
                self.submit_record_to_log(record, handler)
            else:
                pass

    def submit_record_to_log(self, record, handler):
        with open(handler.baseFilename, mode='a', encoding='cp1252') as stream:
            handler.stream = stream
            handler.emit(record)
