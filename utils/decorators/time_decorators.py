import functools
from time import time


def not_more_than(normal_duration):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            start_time = time()
            result = func(*args, **kwargs)
            duration = time() - start_time
            assert duration <= normal_duration, f"<{func.__name__}>'s duration is {duration} that is more than {normal_duration}"

            return result

        return wrapper

    return decorator


def timer(func):

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time()
        result = func(*args, **kwargs)
        duration = time() - start_time
        print(f"<{func.__name__}>'s duration is {duration}")
        return result

    return wrapper
