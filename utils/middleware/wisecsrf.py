from django.core import signing

class CSRFHideMiddleware:
    """
    Hide Django from Wappalyzer detector
    """
    
    BLANK = ''
    
    def __init__(self, get_response):
        # One-time configuration and initialization.
        self.get_response = get_response

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        if request.method == "POST":
            request = self.make_request_post_mutable(request)
            csrf_token = request.POST.get('csrfmiddlewaretoken', self.BLANK)
            if csrf_token == self.BLANK:
                request = self.create_token(request)
                request = self.make_request_post_immutable(request)
        response = self.get_response(request)
        return response

    def make_request_post_mutable(self, request):
        if not request.POST._mutable:
                request.POST._mutable = True
        return request

    def make_request_post_immutable(self, request):
        request.POST._mutable = False
        return request

    def create_token(self, request):
        csrf_field = signing.dumps('csrfmiddlewaretoken').partition(':')[0]
        request_csrf_token = request.POST.get(csrf_field, self.BLANK)
        request.POST['csrfmiddlewaretoken'] = request_csrf_token
        return request
