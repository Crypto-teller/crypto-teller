from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib import auth
from django.shortcuts import render, redirect, get_object_or_404
# from .models import User


def home(request):
    ss =User.objects
    return render(request,'bot/signup.html',{'ss':ss})


# Create your views here.
def signup(request):
    if request.method == 'POST':
        if request.POST['password1'] == request.POST['password2']:
            try:
                user = User.objects.get(username=request.POST['username'])
                return render(request, 'bot/signup.html', {'error': 'this username has been created before'})
            except User.DoesNotExist:
                user = User.objects.create_user(username=request.POST['username']
                  , password=request.POST['password1'], first_name=request.POST['first_name']
                 , last_name=request.POST['last_name'], email=request.POST['email'])
                auth.login(request, user)
                return redirect('tamam')
        else:
            return render(request, 'bot/signup.html', {'error': 'رمز عبور ها باید یکی باشند '})
    else:
        return render(request, 'bot/signup.html')

# def login(request):
#     if request.method == 'POST':
#         user = auth.authenticate(username=request.POST['username'],password=request.POST['password1'])
#         if user is not None:
#             auth.login(request, user)
#             return redirect('signup')
#         else:
#             return render(request, 'bot/login.html',{'error':'نام کاربری یا رمز عبور اشتباه است '})
#     else:
#         return render(request, 'bot/login.html')
# @permission_required('take_createddate')

# def done(request):
#         return render(request, 'done.html')

def tamam(request):
        return render(request, 'bot/tamam.html')
