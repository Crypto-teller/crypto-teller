from django.contrib import admin
from . import models
from django_jalali.admin.filters import JDateFieldListFilter
# Register your models here.

# admin.site.register(models.User)
# class UserAdmin(admin.ModelAdmin):
#     list_display=('UserName','first_name','last_name','email','id')

# Register your models here.
# admin.site.register(models.BotUser)
# class BotUserAdmin(admin.ModelAdmin):
#     list_display=('UserName','first_name','last_name','email','id')

admin.site.register(models.channel)
class channelAdmin(admin.ModelAdmin):
    list_display=(
        'id',
        'Name',
        'created',
        'users'
    )

# admin.site.register(models.Group)
# class GroupAdmin(admin.ModelAdmin):
#     list_display=(
#         'id',
#         'Name',
#         'created',
#         'users'
#     )

admin.site.register(models.BotUser)
class GroupAdmin(admin.ModelAdmin):
    list_display=(
        'id',
        'UserName',
        'first_name',
        'last_name'
        'email',
        ('date', JDateFieldListFilter),
        'objects',
    )
