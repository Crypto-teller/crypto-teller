from django.db import models
from django.conf import settings
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.core.validators import (
    MaxLengthValidator,
    MinLengthValidator,
    FileExtensionValidator,
    EmailValidator,
    RegexValidator,
)
from django_jalali.db import models as jmodels
from django.utils.translation import gettext_lazy as _
from utils.regex import (
    phone_number,
    characters
)
# User = settings.AUTH_USER_MODEL

# Create your models here.
class channel(models.Model):
    id=models.IntegerField(primary_key=True)
    Name=models.CharField(max_length=300)
    # permissions=models.permissions()

class BotUser(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(
        _('Username'),
        max_length=30,
    )
    
    email = models.EmailField(
        _('Email Address'),
        unique=True,
        validators=[
            EmailValidator
        ]
    )
    # notify_when_available = models.ManyToManyField(
    #     to=Pack,
    #     verbose_name=_('Notify When Available'),
    #     related_name='notify_users',
    # )
    phone_number = models.CharField(
        _('Phone Number'),
        max_length=15,
        validators=[
            MinLengthValidator(11),
            MaxLengthValidator(15),
            RegexValidator(phone_number.PERSIAN_PHONE_NUMBER_PATTERN)
        ],
        unique = True
    )

    first_name = models.CharField(
        _('First Name'),
        max_length=30,
        blank=True,
        null=True,
        validators=[
            MinLengthValidator(3),
            MaxLengthValidator(30),
            RegexValidator(
                characters.NATIONAL_CHARS,
                message=_("First Name must have alphabetic characters.")
            ),
        ]
    )

    last_name = models.CharField(
        _('Last Name'),
        max_length=30,
        blank=True,
        null=True,
        validators=[RegexValidator(
            characters.NATIONAL_CHARS,
            message=_("Last Name must have alphabetic characters.")
        )]
    )

    objects = jmodels.jManager()
    date = jmodels.jDateField()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['phone_number']

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')
        ordering = ['id']

    def __str__(self):
        return "%s, %s" % (self.username, self.date)
